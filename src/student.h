//
//  student.h
//  Class Roster
//
//  Created by Jonathan Mecham on 5/22/20.
//  Copyright © 2020 Jonathan Mecham. All rights reserved.
//

#ifndef student_h
#define student_h
#include "degree.h"
#include <iostream>

using namespace std;

class Student {
    private:
    //student class variables
        string studentID;
        string firstName;
        string lastName;
        string emailAddress;
        int age;
        int* daysLeft;
        DegreeProgram degreeProgram;
        const static int daysLeftSize = 3;

    
    //stuent class functions
    public:
        Student();
        Student(string studentID, string firstName, string lastName, string emailAddress, int age, int daysLeft[], DegreeProgram degreeProgram);
    

        //student class getters
        string getStudentID();
        string getFirstName();
        string getLastName();
        string getEmailAddress();
        int getAge();
        int* getDaysLeft();
        virtual DegreeProgram getDegreeProgram();
    
        //student class setters
        void setStudentID(string studentID);
        void setFirstName(string firstName);
        void setLastName(string lastName);
        void setEmailAdress(string emailAddress);
        void setAge(int age);
        void setDaysLeft(int daysLeft[]);
        virtual void setDegree(DegreeProgram degree);
    
        //other student functions
        virtual void print();
        
};

#endif /* student_h */
