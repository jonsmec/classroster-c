//
//  roster.h
//  Class Roster
//
//  Created by Jonathan Mecham on 6/1/20.
//  Copyright © 2020 Jonathan Mecham. All rights reserved.
//
#include "degree.h"
#include "student.h"
#include <iostream>
#ifndef roster_h
#define roster_h
using namespace std;

//inputed student data
    const string studentData[] =
{"A1,John,Smith,John1989@gm ail.com,20,30,35,40,SECURITY", "A2,Suzan,Erickson,Erickson_1990@gmailcom,19,50,30,40,NETWORK", "A3,Jack,Napoli,The_lawyer99yahoo.com,19,20,40,33,SOFTWARE", "A4,cyd,sheffy,Erin.black@comcast.net,22,50,58,40,SECURITY", "A5,Jonathan,Mecham,jmecha1@wgu.edu,24,12,34,56,SOFTWARE"};
    
    const static int columns = 9;
    const static int totalStudents = 5;

class Roster {
    public:
        //constructor
        Roster();
    
        Student* classRosterArray[totalStudents] = {NULL, NULL, NULL, NULL, NULL};
        
        void add(string studentID, string firstName, string lastName, string emailAddress, int age, int daysInCourse1, int daysInCourse2, int daysInCourse3, DegreeProgram degreeprogram);
        void remove(string studentID);
        void printAll();
        void printAverageDaysInCourse(string studentID);
        void printInvalidEmails();
        void printByDegreeProgram(DegreeProgram degreeProgram);
   
        ~Roster();
};



#endif /* roster_h */
