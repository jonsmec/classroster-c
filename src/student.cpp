//
//  student.cpp
//  Class Roster
//
//  Created by Jonathan Mecham on 5/22/20.
//  Copyright © 2020 Jonathan Mecham. All rights reserved.
//

#include "student.h"
#include "degree.h"
#include <iostream>
#include <iomanip>
using namespace std;

Student::Student(){
    this->studentID = "studentID";
    this->firstName = "firstName";
    this->lastName = "lastName";
    this->emailAddress = "emailAdress";
    this->age = 0;
    this->daysLeft = new int[daysLeftSize];
    for (int i = 0; i < 3; i++) {
        this->daysLeft[i] = 0;
    }
}

Student::Student(string sID, string fName, string lName, string eAddress, int a, int daysLeft[], DegreeProgram degree) {
    
    this->studentID = sID;
    this->firstName = fName;
    this->lastName = lName;
    this->emailAddress = eAddress;
    this->age = a;
    this->daysLeft = new int[daysLeftSize];
    for (int i = 0; i < 3; ++i) {
        this->daysLeft[i] = daysLeft[i];
    }
}
    
//student class getters
    string Student::getStudentID() {
        return studentID;
    }
    string Student::getFirstName() {
        return firstName;
    }
    string Student::getLastName() {
        return lastName;
    }
    string Student::getEmailAddress() {
        return emailAddress;
    }
    int Student::getAge() {
        return age;
    }
    //FIXME may have logic error
    int* Student::getDaysLeft(){
        return daysLeft;
    }
    DegreeProgram Student::getDegreeProgram() {
        return degreeProgram;
    }

//student class setters
    void Student::setStudentID(string newSID){
        this->studentID = newSID;
    }
    void Student::setFirstName(string newFN){
        this->firstName = newFN;
    }
    void Student::setLastName(string newLN){
        this->lastName = newLN;
    }
    void Student::setEmailAdress(string newEA){
        this->emailAddress = newEA;
    }
    void Student::setAge(int newAge){
        this->age = newAge;
    }
    void Student::setDaysLeft(int* days){
        for (int i = 0; i < 3; i++) {
        this->daysLeft[i] = days[i];
        }
    }
    void Student::setDegree(DegreeProgram degree){
        this->degreeProgram = degree;
    }

void Student::print() {
    int* daysLeft = getDaysLeft();
    std::cout << left << setw(5) << getStudentID()
        << "First Name: "<< setw(10) << getFirstName()
        << "Last Name: " << setw(10) << getLastName()
        << "Age: " << setw(5) << getAge()
        << "daysInCourse: "
        << "{" << *daysLeft << ", " << *(daysLeft + 1) << ", " << *(daysLeft + 2) << "}"
        << "\t Degree Program: ";
    
    //checks the degree program of the student and prints the string accordingly 
    switch (getDegreeProgram()){
        case SECURITY: {
            std::cout << "SECURITY";
            break;
        }
        case NETWORK: {
            std::cout << "NETWORK";
            break;
        }
        case SOFTWARE: {
            std::cout << "SOFTWARE";
            break;
        }
    }
    std::cout << endl;
}
    
    
    

