//
//  roster.cpp
//  Class Roster
//
//  Created by Jonathan Mecham on 6/1/20.
//  Copyright © 2020 Jonathan Mecham. All rights reserved.
//

#include "roster.h"
#include "student.h"
#include <sstream>

using namespace std;


Roster::Roster(){}

void Roster::add(string studentID, string firstName, string lastName, string emailAddress, int age, int daysInCourse1, int daysInCourse2, int daysInCourse3, DegreeProgram degreeprogram) {
    //put daysInCourse1,2,3 into an array
    int daysLeft[] = {daysInCourse1, daysInCourse2, daysInCourse3};
    //create new student object with entered parameters
    Student* student;
    student = new Student(studentID, firstName, lastName, emailAddress, age,daysLeft, degreeprogram);
    
    if (degreeprogram == NETWORK) {
        student->setDegree(NETWORK);
    }
    else if (degreeprogram == SECURITY) {
        student->setDegree(SECURITY);
        }
    else if (degreeprogram == SOFTWARE) {
        student->setDegree(SOFTWARE);
    }
    
    //add student to the roster array
    for (int i = 0; i < totalStudents; i++){
        if(classRosterArray[i] == NULL) {
            classRosterArray[i] = student;
            //exit out of loop so it doesn't add student multiple times
            break;
        }
    }
}



// removes students from roster by their ID. If the ID is not found, prints error message
void Roster::remove(string studentID) {
    for (int i = 0; i < totalStudents; i++) {
        //if classRosterArray[i] is == NULL, then the student was never added
        if (classRosterArray[i] == NULL){
            std::cout << "The student with the ID: " << studentID << " was not found" << endl;
            break;
        }
        else if (studentID == classRosterArray[i]->getStudentID()){
            classRosterArray[i] = NULL;
           cout << "Student ID: " << studentID << " removed:" << endl;
        }
    }
}

// loops through all students calling the print function
void Roster::printAll(){
    cout << "Displaying all students:" << endl;
    for (int i = 0; i < totalStudents; i++) {
        if (classRosterArray[i] == NULL) {
            continue;
        }
        else {
            this->classRosterArray[i]->print();
        }
    }
    cout << endl;
}

// prints the averge days a student has in course.
void Roster::printAverageDaysInCourse(string studentID){
    for (int i = 0; i < totalStudents; i ++) {
        if (this->classRosterArray[i]->getStudentID() == studentID){
            int* days = classRosterArray[i]->getDaysLeft();
            std::cout << "Student ID: " << studentID << ", averages " << (days[0] + days[1] + days[2])/3;
            std::cout << endl;
        }
    }
}

//verifys student's email addresses and displays all invalid email addresses
//Note: A valid email should include an at sign ('@') and period ('.') and should not include a space (' ').
void Roster::printInvalidEmails() {
    cout << "Displaying invalid emails:" << endl;

    //loops through email of each student
    for (int i = 0; i < totalStudents; i++) {
        string email = classRosterArray[i]->getEmailAddress();
        //checks the validity of the email
        if (email.find("@") == string::npos){
            std::cout << classRosterArray[i]->getEmailAddress() << " - missing an @ symbol" << endl;
        }
        if (email.find(".") == string::npos){
            std::cout << classRosterArray[i]->getEmailAddress() << " - missing a period" << endl;
        }
        if (email.find(" ") != string::npos){
            std::cout << classRosterArray[i]->getEmailAddress() << " - no spaces allowed" << endl;
        }
    }
    cout << endl;
}

//searches for students with same degree program and prints them
void Roster::printByDegreeProgram(DegreeProgram degreeProgram){
    string degree;
    if (degreeProgram == NETWORK) {
        degree = "NETWORK";
    }
    else if (degreeProgram == SECURITY) {
        degree = "SECURITY";
        }
    else if (degreeProgram == SOFTWARE) {
        degree = "SOFTWARE";
    }
    cout << endl << "Displaying students in degree program: " << degree << endl;
    for (int i = 0; i < totalStudents; i++) {
        if (this->classRosterArray[i]->getDegreeProgram() == degreeProgram){
            this->classRosterArray[i]->print();
        }
    }
    cout << endl;
}


Roster::~Roster(){
    
}

