//
//  main.cpp
//  Class Roster
//
//  Created by Jonathan Mecham on 5/22/20.
//  Copyright © 2020 Jonathan Mecham. All rights reserved.
//

#include "student.h"
#include "roster.h"
#include <sstream>
#include <string>
#include <iomanip>
#include <iostream>
using namespace std;

int main() {

    std::cout << "C867: Scripting & Programming - Applications" << endl;
    std::cout << "Programming Language: C++" << endl;
    std::cout << "Student ID: 001311375" << endl;
    std::cout << "Name: Jonathan Mecham" << endl << endl;
    
    Roster classRoster;
    
    // read student data in from studentData array ((
    DegreeProgram degree = NETWORK;
    string dataOut;
    string dataColumn[9];
        for (int i = 0; i < totalStudents; i++) {
        istringstream input(studentData[i]);
        for (int j = 0; j < 9; j++){
            getline(input, dataOut, ',');
            dataColumn[j] = dataOut;
            }

            if (dataColumn[8].compare("SECURITY") == 0){
                        degree = SECURITY;}
            else if (dataColumn[8].compare("NETWORK") == 0) {
                        degree = NETWORK;}
            else if (dataColumn[8].compare("SOFTWARE") == 0) {
                        degree = SOFTWARE;}

        classRoster.add(dataColumn[0], dataColumn[1], dataColumn[2], dataColumn[3], stoi(dataColumn[4]), stoi(dataColumn[5]), stoi(dataColumn[6]), stoi(dataColumn[7]), degree);
    }
    // ))
    
    classRoster.printAll();
    classRoster.printInvalidEmails();
    
    for (int i = 0; i < totalStudents; i++){ classRoster.printAverageDaysInCourse(classRoster.classRosterArray[i]->getStudentID());}
    
    classRoster.printByDegreeProgram(SOFTWARE);
    classRoster.remove("A3");
    classRoster.printAll();
    classRoster.remove("A3");
    
    classRoster.~Roster();
    
    
    }
    
